package ru.tinkoff.j2s.library.repository.users

import org.scalatest.funsuite.AnyFunSuite
import cats.effect.IO
import doobie.scalatest.IOChecker
import doobie.util.transactor.Transactor

import ru.tinkoff.j2s.library.domain.users.User
import ru.tinkoff.j2s.library.domain.auth.Role
import ru.tinkoff.j2s.library.domain.Doobie

class UserQueryTypeCheckSpec extends AnyFunSuite with IOChecker {
  override val transactor: Transactor[IO] = Doobie.testTransactor

  import UserSQL._

  val user = User(1L, Role.Admin, "Vasya1337", "Vasya", "Pupkin", "vasya1337@gmail.com", "etohash", "88003353535")

  test("insert") { check {
    insert(user.role, user.userName, user.firstName, user.lastName, user.email, user.hash, user.phone)
  }}
  test("update")     { check { update(user) }}
  test("select")     { check { select(user.id) }}
  test("byUserName") { check { byUserName(user.userName) }}
  test("delete")     { check { delete(user.id) }}
  test("selectAll")  { check { selectAll(10, 0) }}
}
