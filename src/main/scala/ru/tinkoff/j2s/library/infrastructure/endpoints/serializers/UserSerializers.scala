package ru.tinkoff.j2s.library.infrastructure.endpoints.serializers

import io.circe._
import io.circe.generic.semiauto._

import ru.tinkoff.j2s.library.domain.users.User

object UserSerializers {
  implicit val userDecoder: Decoder[User]  = deriveDecoder
  implicit val userEncoder: Encoder[User]  = deriveEncoder
}
