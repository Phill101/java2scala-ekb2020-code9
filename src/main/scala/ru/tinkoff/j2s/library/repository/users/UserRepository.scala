package ru.tinkoff.j2s.library.repository.users

import cats.data.OptionT

import ru.tinkoff.j2s.library.domain.auth.Role
import ru.tinkoff.j2s.library.domain.users.User

trait UserRepository[F[_]] {
  def create(
    role: Role, 
    userName: String,
    firstName: String,
    lastName: String,
    email: String,
    hash: String,
    phone: String
  ): F[User]

  def update(user: User): F[User]

  def get(id: Long): OptionT[F, User]

  def delete(userId: Long): OptionT[F, User]

  def findByUserName(userName: String): OptionT[F, User]

  def deleteByUserName(userName: String): OptionT[F, User]

  def list(pageSize: Int, offset: Int): F[List[User]]
}
