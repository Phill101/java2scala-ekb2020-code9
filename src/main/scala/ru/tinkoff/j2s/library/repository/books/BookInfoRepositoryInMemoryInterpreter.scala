package ru.tinkoff.j2s.library.repository.books

import cats.Applicative
import cats.data.OptionT
import cats.instances.string._
import cats.syntax.applicative._
import cats.syntax.eq._

import ru.tinkoff.j2s.library.domain.books.BookInfo

class BookInfoRepositoryInMemoryInterpreter[F[_]: Applicative] extends BookInfoRepository[F] {
  private val books = List(
    BookInfo("456", "Королевства и Магичности", List("Нижников О"), "Невероятно позновательный рассказ")
  )

  def findByIsbn(isbn: String): OptionT[F, BookInfo] =
    OptionT(books.find(_.isbn === isbn).pure[F])
}
