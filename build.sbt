import Dependencies._

ThisBuild / scalaVersion     := "2.13.1"
ThisBuild / version          := "0.1.0"
ThisBuild / organization     := "ru.tinkoff"
ThisBuild / organizationName := "tinkoff.ru"

lazy val root = (project in file("."))
  .enablePlugins(BuildInfoPlugin)
  .settings(
    name := "fp-library",
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      git.branch,
      git.gitHeadCommit,
      git.gitHeadCommitDate,
      git.gitCurrentBranch,
      scalaVersion,
      sbtVersion,
    ),
    buildInfoPackage := "ru.tinkoff.j2s.library.buildinfo",
    buildInfoOptions += BuildInfoOption.ToJson,
    libraryDependencies ++= all
  )
