package ru.tinkoff.j2s.library.service.users

import cats.{Functor, Monad}
import cats.data._
import cats.syntax.functor._

import ru.tinkoff.j2s.library.domain.auth.Role
import ru.tinkoff.j2s.library.domain.users.User
import ru.tinkoff.j2s.library.domain.users.errors._
import ru.tinkoff.j2s.library.repository.users.UserRepository

class UserService[F[_]](userRepo: UserRepository[F], validation: UserValidation[F]) {

  def createUser(
    role: Role,
    userName: String,
    firstName: String,
    lastName: String,
    email: String,
    hash: String,
    phone: String
  )(implicit M: Monad[F]): EitherT[F, UserAlreadyExistsError, User] =
    for {
      _     <- validation.userNameDoesNotExist(userName)
      saved <- EitherT.liftF(userRepo.create(role, userName, firstName, lastName, email, hash, phone))
    } yield saved

  def getUser(userId: Long)(implicit F: Functor[F]): EitherT[F, UserNotFoundError.type, User] =
    userRepo.get(userId).toRight(UserNotFoundError)

  def getUserByName(userName: String)(implicit F: Functor[F]): EitherT[F, UserNotFoundError.type, User] =
    userRepo.findByUserName(userName).toRight(UserNotFoundError)

  def deleteUser(userId: Long)(implicit F: Functor[F]): F[Unit] =
    userRepo.delete(userId).value.void

  def deleteByUserName(userName: String)(implicit F: Functor[F]): F[Unit] =
    userRepo.deleteByUserName(userName).value.void

  def update(user: User)(implicit M: Monad[F]): EitherT[F, UserNotFoundError.type, User] =
    for {
      _     <- validation.exists(user.id)
      saved <- EitherT.liftF(userRepo.update(user))
    } yield saved

  def list(pageSize: Int, offset: Int): F[List[User]] =
    userRepo.list(pageSize, offset)
}

object UserService {
  def apply[F[_]](
      repository: UserRepository[F],
      validation: UserValidation[F],
  ): UserService[F] =
    new UserService[F](repository, validation)
}
