package ru.tinkoff.j2s.library.domain

import cats.implicits._
import cats.effect.{Async, ContextShift, Effect, IO}
import _root_.doobie.Transactor

import ru.tinkoff.j2s.library.infrastructure.config.{Config, DatabaseConfig}

import scala.concurrent.ExecutionContext
import ru.tinkoff.j2s.library.infrastructure.backend.database.DatabaseModule
import ru.tinkoff.j2s.library.infrastructure.config.ConfigModule
import cats.effect.Blocker
import cats.effect.Resource

object Doobie {
  def getTransactor[F[_]: Async: ContextShift](cfg: DatabaseConfig): Transactor[F] =
    Transactor.fromDriverManager[F](
      cfg.driver,
      cfg.url,
      cfg.user,
      cfg.password
    )

  import pureconfig._
  import pureconfig.generic.auto._
  import ru.tinkoff.j2s.library.infrastructure.config.Config
  val config = ConfigSource.defaultReference.loadOrThrow[Config]

  /*
   * Provide a transactor for testing once schema has been migrated.
   */
  def initializedTransactor[F[_]: Effect: Async: ContextShift]: F[Transactor[F]] =
    for {
      _ <- DatabaseModule.migrate(config.db)
    } yield getTransactor(config.db)

  lazy val testEc = ExecutionContext.Implicits.global

  implicit lazy val testCs = IO.contextShift(testEc)

  lazy val testTransactor = initializedTransactor[IO].unsafeRunSync()
}
