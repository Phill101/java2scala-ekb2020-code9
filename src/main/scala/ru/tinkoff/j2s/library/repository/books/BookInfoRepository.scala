package ru.tinkoff.j2s.library.repository.books

import cats.data.OptionT

import ru.tinkoff.j2s.library.domain.books.BookInfo

trait BookInfoRepository[F[_]] {
  def findByIsbn(isbn: String): OptionT[F, BookInfo]
}
