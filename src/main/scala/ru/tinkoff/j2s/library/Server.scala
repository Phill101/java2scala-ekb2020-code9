package ru.tinkoff.j2s.library

import cats.effect._
import cats.syntax.all._

import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder

import sttp.client.SttpBackend
import sttp.client.asynchttpclient.WebSocketHandler
import sttp.client.asynchttpclient.cats.AsyncHttpClientCatsBackend

import tsec.authentication.SecuredRequestHandler
import tsec.mac.jca.HMACSHA256
import tsec.passwordhashers.jca.SCrypt


import ru.tinkoff.j2s.library.infrastructure.backend.database.DatabaseModule
import ru.tinkoff.j2s.library.infrastructure.config.ConfigModule
import ru.tinkoff.j2s.library.infrastructure.endpoints.{Auth, AuthEndpoints}
import ru.tinkoff.j2s.library.infrastructure.endpoints.{BookEndpoints, UserEndpoints, SystemEndpoints}
import ru.tinkoff.j2s.library.infrastructure.logging.{Log, LogbackLog}

import ru.tinkoff.j2s.library.service.books.BookService
import ru.tinkoff.j2s.library.service.users.{UserService, UserValidationInterpreter}

import ru.tinkoff.j2s.library.repository.auth.AuthRepositoryDoobieInterpreter
import ru.tinkoff.j2s.library.repository.books.{BookInfoRepositoryHttpInterpreter, BookInfoRepositoryInMemoryInterpreter}
import ru.tinkoff.j2s.library.repository.users.{UserRepositoryDoobieInterpreter, UserRepositoryInMemoryInterpreter}


object Server extends IOApp {
  
  def init[F[_] : Timer : ConcurrentEffect : ContextShift](l: Log[F]) =
    for {
      _ <- Resource.liftF(l.info("starting"))
      config <- Blocker[F].evalMap(ConfigModule[F])

      //userRepository <- Resource.liftF(UserRepositoryInMemoryInterpreter[F])

      tx <- DatabaseModule[F](config.db)

      key <- Resource.liftF(HMACSHA256.generateKey[F])
      authRepository = AuthRepositoryDoobieInterpreter[F, HMACSHA256](key, tx)
      
      userRepository  = UserRepositoryDoobieInterpreter(tx)
      userValidation  = UserValidationInterpreter(userRepository)
      userService     = UserService(userRepository, userValidation)

      //bookRepository = new BookRepositoryInMemoryInterpreter[F]

      implicit0(
        httpBackend: SttpBackend[F, Nothing, WebSocketHandler]
      ) <- AsyncHttpClientCatsBackend.resource[F]()
      bookRepository <- Resource.liftF(LogbackLog[F].forService[BookInfoRepositoryHttpInterpreter[F]]
                                                    .map(new BookInfoRepositoryHttpInterpreter[F](config.googleBooks, _)))

      bookService    = BookService(bookRepository)

      authenticator = Auth.jwtAuthenticator(key, authRepository, userRepository)
      routeAuth = SecuredRequestHandler(authenticator)
      router = Router(
        "/system" -> SystemEndpoints.endpoints[F],
        "/auth"   -> AuthEndpoints.endpoints[F, SCrypt, HMACSHA256](userService, SCrypt.syncPasswordHasher[F], routeAuth),
        "/user"   -> UserEndpoints.endpoints(userService, routeAuth),
        "/book"   -> BookEndpoints.endpoints(bookService)
      ).orNotFound

      _      <- Resource.liftF(DatabaseModule.migrate[F](config.db))
      server <- BlazeServerBuilder[F]
                  .bindHttp(port = 8080)
                  .withHttpApp(router)
                  .resource
    } yield server

  override def run(args: List[String]): IO[ExitCode] =
    for {
      l <- LogbackLog[IO].forName("init")
      _ <- init(l).use(_ => IO.never)
    } yield ExitCode.Success
}
