package ru.tinkoff.j2s.library.service.books

import cats.{Functor, Monad}
import cats.data._
import cats.syntax.functor._

import ru.tinkoff.j2s.library.domain.books.BookInfo
import ru.tinkoff.j2s.library.repository.books.BookInfoRepository

class BookService[F[_]](bookInfoRepo: BookInfoRepository[F]) {
  def findByIsbn(isbn: String): OptionT[F, BookInfo] =
    bookInfoRepo.findByIsbn(isbn)
}

object BookService {
  def apply[F[_]](infoRepo: BookInfoRepository[F]): BookService[F] =
    new BookService[F](infoRepo)
}
