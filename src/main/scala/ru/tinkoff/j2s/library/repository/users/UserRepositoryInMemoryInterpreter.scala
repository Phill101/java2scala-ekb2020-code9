package ru.tinkoff.j2s.library.repository.users

import java.util.Random

import cats.implicits._
import cats.Applicative
import cats.data.OptionT
import cats.effect.Sync
import cats.effect.concurrent.Ref

import tsec.authentication.IdentityStore

import ru.tinkoff.j2s.library.domain.auth.Role
import ru.tinkoff.j2s.library.domain.users.User

class UserRepositoryInMemoryInterpreter[F[_]: Applicative](
  ref: Ref[F, Map[Long, User]]
) extends UserRepository[F] with IdentityStore[F, Long, User] {

  private val random = new Random

  def create(
    role: Role, 
    userName: String,
    firstName: String,
    lastName: String,
    email: String,
    hash: String,
    phone: String
  ): F[User] = {
    val id = random.nextLong
    val toSave = User(id, role, userName, firstName, lastName, email, hash, phone)
    ref.update(_ + (id -> toSave)).as(toSave)
  }

  def update(user: User): F[User] =
    ref.update(_ + (user.id -> user)).as(user)

  def get(id: Long): OptionT[F, User] =
    OptionT(ref.get.map(_.get(id)))

  def delete(id: Long): OptionT[F, User] =
    OptionT(ref.modify(m => (m.removed(id), m.get(id))))

  def findByUserName(userName: String): OptionT[F, User] =
    OptionT(ref.get.map(_.values.find(_.userName === userName)))

  def list(pageSize: Int, offset: Int): F[List[User]] =
    ref.get.map(_.values.toList.sortBy(_.lastName).slice(offset, offset + pageSize))

  def deleteByUserName(userName: String): OptionT[F, User] =
    OptionT(ref.modify { m => 
      val optUser = m.values.find(_.userName === userName)
      optUser.map(user => (m.removed(user.id), user.some))
             .getOrElse((m, none))
    })
}

object UserRepositoryInMemoryInterpreter {
  def apply[F[_]: Sync] = 
    Ref.of[F, Map[Long, User]](Map.empty)
       .map(new UserRepositoryInMemoryInterpreter[F](_))
}
