package ru.tinkoff.j2s.library.repository.books

import cats._
import cats.data.{OptionT, NonEmptyList}
import cats.instances.either._
import cats.syntax.all._

import enumeratum._
import enumeratum.EnumEntry._

import io.circe._
import io.circe.generic.semiauto._

import sttp.client._
import sttp.client.circe._
import sttp.model._

import ru.tinkoff.j2s.library.domain.books.BookInfo
import ru.tinkoff.j2s.library.infrastructure.config.GoogleBooksConfig
import ru.tinkoff.j2s.library.infrastructure.logging.Log

import scala.concurrent.duration._

class BookInfoRepositoryHttpInterpreter[F[_]: Monad](conf: GoogleBooksConfig, l: Log[F])(
  implicit sttpBackend: SttpBackend[F, Nothing, NothingT]
) extends BookInfoRepository[F] {
  import BookRepositoryHttpInterpreter._

  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  def findByIsbn(isbn: String): OptionT[F, BookInfo] = {
    OptionT(
      basicRequest.header(Header.accept(MediaType.ApplicationJson.toString))
                  .readTimeout(conf.readTimeout)
                  .get(uri"https://www.googleapis.com/books/v1/volumes?q=${"isbn:" + isbn}")
                  .response(asJson[GoogleBookByIsbnResponse])
                  .send[F]
                  .flatMap(response =>
                    response.body.fold(
                      {
                        case HttpError(body) => 
                          l.error(s"HttpError($body)").as(None)
                        case DeserializationError(body, error) =>
                          l.error(s"DeserializationError($body, $error)").as(None)
                      },
                      {
                        case GoogleBookByIsbnResponse(_, Nil) =>
                          l.debug("book not found by isbn($isbn)").as(none)
                        case GoogleBookByIsbnResponse(_, info :: _) =>
                          val book =
                            BookInfo(
                              isbn        = info.volumeInfo.industryIdentifiers.head.identifier,
                              title       = info.volumeInfo.title,
                              authors     = info.volumeInfo.authors,
                              description = info.volumeInfo.description
                            )
                          l.debug(s"found book by isbn($isbn): $book").as(book.some)
                      }
                    )
                  )
    )
  }
}

object BookRepositoryHttpInterpreter {

  sealed trait IdentityType extends UpperSnakecase 
  object IdentityType extends Enum[IdentityType] with CirceEnum[IdentityType] {
    case object Isbn13 extends IdentityType {
      override val entryName: String = "ISBN_13"
    }
    case object Isbn_10 extends IdentityType

    val values = findValues
  }
  case class IndustryIdentifier(
    `type`: IdentityType,
    identifier: String
  )
  case class GoogleBookVolumeInfo(
    title: String,
    authors: List[String],
    publisher: String,
    publishedDate: String,
    description: String,
    industryIdentifiers: NonEmptyList[IndustryIdentifier]
  )
  case class GoogleBookItem(
    id: String,
    volumeInfo: GoogleBookVolumeInfo
  )
  case class GoogleBookByIsbnResponse(
    totalItems: Int,
    items: List[GoogleBookItem]
  )

  implicit val industryIdentifierDecoder: Decoder[IndustryIdentifier]             = deriveDecoder
  implicit val googleBookVolumeInfoDecoder: Decoder[GoogleBookVolumeInfo]         = deriveDecoder
  implicit val googleBookItemDecoder: Decoder[GoogleBookItem]                     = deriveDecoder
  implicit val GoogleBookByIsbnResponseDecoder: Decoder[GoogleBookByIsbnResponse] = deriveDecoder
}
