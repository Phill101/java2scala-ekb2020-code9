package ru.tinkoff.j2s.library.infrastructure.logging

trait Log[F[_]] {
  def trace(msg: String): F[Unit]
  def trace(msg: String, th: Throwable): F[Unit]

  def debug(msg: String): F[Unit]
  def debug(msg: String, th: Throwable): F[Unit]

  def info(msg: String): F[Unit]
  def info(msg: String, th: Throwable): F[Unit]

  def warn(msg: String): F[Unit]
  def warn(msg: String, th: Throwable): F[Unit]

  def error(msg: String): F[Unit]
  def error(msg: String, th: Throwable): F[Unit]
}
