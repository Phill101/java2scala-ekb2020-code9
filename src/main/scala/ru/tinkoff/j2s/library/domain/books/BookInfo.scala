package ru.tinkoff.j2s.library.domain.books

case class BookInfo(isbn: String, title: String, authors: List[String], description: String)
