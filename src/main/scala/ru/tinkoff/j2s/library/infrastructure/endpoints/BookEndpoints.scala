package ru.tinkoff.j2s.library.infrastructure.endpoints

import cats.effect.Sync
import cats.syntax.functor._
import cats.syntax.flatMap._

import io.circe.generic.auto._
import io.circe.syntax._

import org.http4s._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

import tsec.jwt.algorithms.JWTMacAlgo
import tsec.authentication._

import ru.tinkoff.j2s.library.domain.books.BookInfo
import ru.tinkoff.j2s.library.service.books.BookService
import ru.tinkoff.j2s.library.infrastructure.endpoints.Auth

class BookEndpoints[F[_]: Sync] extends Http4sDsl[F] {

  implicit val bookDecoder: EntityDecoder[F, BookInfo] = jsonOf[F, BookInfo]

  object IsbnQueryParamMatcher extends QueryParamDecoderMatcher[String]("isbn")
  private def getBookByIsbn(bookService: BookService[F]): PartialFunction[Request[F], F[Response[F]]] = {
    case GET -> Root :? IsbnQueryParamMatcher(isbn) =>
      bookService.findByIsbn(isbn).value.flatMap {
        case Some(book) => Ok(book.asJson)
        case None       => NotFound("Book was not found")
      }
  }

  def endpoints(bookService: BookService[F]): HttpRoutes[F] = {
    val endpoints = 
        getBookByIsbn(bookService)

    HttpRoutes.of[F](endpoints)
  }
}

object BookEndpoints {
  def endpoints[F[_]: Sync](bookService: BookService[F]): HttpRoutes[F] =
    new BookEndpoints[F].endpoints(bookService)
}
