package ru.tinkoff.j2s.library.infrastructure.endpoints

import cats.effect.Sync

import io.circe.{Encoder, Json}
import io.circe.generic.semiauto._
import io.circe.syntax._

import org.http4s.{Request, Response, HttpRoutes, EntityDecoder, MediaType}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.`Content-Type`

import ru.tinkoff.j2s.library.buildinfo.BuildInfo

class SystemEndpoints[F[_]: Sync] extends Http4sDsl[F] {
  import SystemEndpoints._

  private def pingEndpoint: PartialFunction[Request[F], F[Response[F]]] = {
    case GET -> Root / "ping" => Ok("pong")
  }

  private def livenessEndpoint: PartialFunction[Request[F], F[Response[F]]] = {
    case GET -> Root / "liveness" => Ok(Liveness(isAlive = true).asJson)
  }

  private def versionEndpoint: PartialFunction[Request[F], F[Response[F]]] = {
    case GET -> Root / "version"  => Ok(BuildInfo.toJson, `Content-Type`(MediaType.application.json))
  }

  def endpoints: HttpRoutes[F] = {
    val endpoints = 
      pingEndpoint orElse livenessEndpoint orElse versionEndpoint

    HttpRoutes.of[F](endpoints)
  }
}

object SystemEndpoints {
  def endpoints[F[_]: Sync]: HttpRoutes[F] =
    new SystemEndpoints[F].endpoints

  case class Liveness(isAlive: Boolean)
  object Liveness {
    implicit val livenessEncoder: Encoder[Liveness] = deriveEncoder
  }
}
