package ru.tinkoff.j2s.library.infrastructure.endpoints

import cats.data.EitherT
import cats.effect.Sync
import cats.syntax.all._

import io.circe._
import io.circe.generic.semiauto._
import io.circe.syntax._

import org.http4s.{Request, Response, HttpRoutes, EntityDecoder}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

import tsec.common.Verified
import tsec.jwt.algorithms.JWTMacAlgo
import tsec.passwordhashers.{PasswordHash, PasswordHasher}
import tsec.authentication._

import ru.tinkoff.j2s.library.domain.auth.Role
import ru.tinkoff.j2s.library.domain.users.User
import ru.tinkoff.j2s.library.domain.users.errors._
import ru.tinkoff.j2s.library.service.users.UserService

class AuthEndpoints[F[_]: Sync, HashFunc, Auth: JWTMacAlgo] extends Http4sDsl[F] {
  import serializers.UserSerializers._
  import AuthEndpoints._

  implicit val loginReqDecoder: EntityDecoder[F, LoginRequest] = jsonOf
  implicit val signupReqDecoder: EntityDecoder[F, SignupRequest] = jsonOf

  private def loginEndpoint(
    userService: UserService[F],
    cryptService: PasswordHasher[F, HashFunc],
    auth: Authenticator[F, Long, User, AugmentedJWT[Auth, Long]],
  ): HttpRoutes[F] =
    HttpRoutes.of[F] {
      case req @ POST -> Root / "login" =>
        val action = for {
          login <- EitherT.liftF(req.as[LoginRequest])
          name = login.userName
          user <- userService.getUserByName(name).leftMap(_ => UserAuthenticationFailedError(name))
          checkResult <- EitherT.liftF(
            cryptService.checkpw(login.password, PasswordHash[HashFunc](user.hash)),
          )
          _ <- if (checkResult == Verified) EitherT.rightT[F, UserAuthenticationFailedError](())
          else EitherT.leftT[F, User](UserAuthenticationFailedError(name))
          token <- EitherT.right[UserAuthenticationFailedError](auth.create(user.id))
        } yield (user, token)

        action.value.flatMap {
          case Right((user, token)) => Ok(user.asJson).map(auth.embed(_, token))
          case Left(UserAuthenticationFailedError(name)) =>
            BadRequest(s"Authentication failed for user $name")
        }
    }

  private def signupEndpoint(
    userService: UserService[F],
    crypt: PasswordHasher[F, HashFunc],
  ): HttpRoutes[F] =
    HttpRoutes.of[F] {
      case req @ POST -> Root =>
        val action = for {
          signup <- req.as[SignupRequest]
          hash <- crypt.hashpw(signup.password)
          result <- userService.createUser(
            role = signup.role,
            userName = signup.userName,
            firstName = signup.firstName,
            lastName = signup.lastName,
            email = signup.email,
            hash = hash.toString,
            phone = signup.phone
          ).value
        } yield result

        action.flatMap {
          case Right(saved) => Ok(saved.asJson)
          case Left(UserAlreadyExistsError(existing)) =>
            Conflict(s"The user with user name ${existing.userName} already exists")
        }
    }
    
  def endpoints(
    userService: UserService[F],
    cryptService: PasswordHasher[F, HashFunc],
    auth: SecuredRequestHandler[F, Long, User, AugmentedJWT[Auth, Long]]
  ): HttpRoutes[F] =
    loginEndpoint(userService, cryptService, auth.authenticator) <+>
    signupEndpoint(userService, cryptService)
}

object AuthEndpoints {
  def endpoints[F[_]: Sync, HashFunc, Auth: JWTMacAlgo](
    userService: UserService[F],
    cryptService: PasswordHasher[F, HashFunc],
    auth: SecuredRequestHandler[F, Long, User, AugmentedJWT[Auth, Long]]
  ): HttpRoutes[F] =
    new AuthEndpoints[F, HashFunc, Auth].endpoints(userService, cryptService, auth)

  implicit val loginRequestDecoder: Decoder[LoginRequest]   = deriveDecoder
  implicit val signupRequestDecoder: Decoder[SignupRequest] = deriveDecoder

  final case class LoginRequest(
    userName: String,
    password: String
  )

  final case class SignupRequest(
    role: Role,
    userName: String,
    firstName: String,
    lastName: String,
    email: String,
    password: String,
    phone: String
  )
}
