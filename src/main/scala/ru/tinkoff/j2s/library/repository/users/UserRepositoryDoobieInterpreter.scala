package ru.tinkoff.j2s.library.repository.users

import cats.data.OptionT
import cats.syntax.functor._
import cats.syntax.either._
import cats.effect._

import io.circe.parser.decode
import io.circe.syntax._

import doobie._
import doobie.implicits._

import tsec.authentication.IdentityStore

import ru.tinkoff.j2s.library.domain.auth.Role
import ru.tinkoff.j2s.library.domain.users.User

object UserSQL {
  // H2 does not support JSON data type.
  implicit val roleMeta: Meta[Role] =
    Meta[String].timap(decode[Role](_).leftMap(throw _).merge)(_.asJson.toString)

  def insert(
    role: Role,
    userName: String,
    firstName: String,
    lastName: String,
    email: String,
    hash: String,
    phone: String
  ): Update0 = sql"""
    INSERT INTO USERS (ROLE, USER_NAME, FIRST_NAME, LAST_NAME, EMAIL, HASH, PHONE)
    VALUES ($role, $userName, $firstName, $lastName, $email, $hash, $phone)
  """.update

  def update(user: User): Update0 = sql"""
    UPDATE USERS
    SET ROLE = ${user.role}, FIRST_NAME = ${user.firstName}, LAST_NAME = ${user.lastName},
        EMAIL = ${user.email}, HASH = ${user.hash}, PHONE = ${user.phone}
    WHERE ID = ${user.id}
  """.update

  def select(userId: Long): Query0[User] = sql"""
    SELECT ID, ROLE, USER_NAME, FIRST_NAME, LAST_NAME, EMAIL, HASH, PHONE
    FROM USERS
    WHERE ID = $userId
  """.query

  def byUserName(userName: String): Query0[User] = sql"""
    SELECT ID, ROLE, USER_NAME, FIRST_NAME, LAST_NAME, EMAIL, HASH, PHONE
    FROM USERS
    WHERE USER_NAME = $userName
  """.query[User]

  def delete(userId: Long): Update0 = sql"""
    DELETE FROM USERS WHERE ID = $userId
  """.update

  // Doobie's typechecker fails unexpectedly for H2.
  // H2 reports it requires a VARCHAR for limit and offset.
  // ¯\_(ツ)_/¯
  def selectAll(lim: Int, offset: Int): Query0[User] = sql"""
    SELECT ID, ROLE, USER_NAME, FIRST_NAME, LAST_NAME, EMAIL, HASH, PHONE
    FROM USERS
    LIMIT ${lim.toString} OFFSET ${offset.toString}
  """.query

}

class UserRepositoryDoobieInterpreter[F[_]: Bracket[*[_], Throwable]](val xa: Transactor[F])
  extends UserRepository[F] with IdentityStore[F, Long, User]  {

    override def create(
      role: Role,
      userName: String,
      firstName: String,
      lastName: String,
      email: String,
      hash: String,
      phone: String
    ): F[User] =
      UserSQL.insert(role, userName, firstName, lastName, email, hash, phone)
        .withUniqueGeneratedKeys[Long]("ID")
        .map(id => User(id, role, userName, firstName, lastName, email, hash, phone))
        .transact(xa)

    override def update(user: User): F[User] =
      UserSQL.update(user).run.transact(xa).as(user)

    override def get(id: Long): OptionT[F,User] =
      OptionT(UserSQL.select(id).option.transact(xa))

    override def delete(userId: Long): OptionT[F,User] =
      get(userId).semiflatMap(user => 
        UserSQL.delete(userId).run.transact(xa).as(user)
      )

    override def findByUserName(userName: String): OptionT[F,User] =
      OptionT(UserSQL.byUserName(userName).option.transact(xa))

    override def deleteByUserName(userName: String): OptionT[F,User] =
      findByUserName(userName).map(_.id).flatMap(delete)

    override def list(pageSize: Int, offset: Int): F[List[User]] =
      UserSQL.selectAll(pageSize, offset).to[List].transact(xa)
}

object UserRepositoryDoobieInterpreter {
  def apply[F[_]: Bracket[*[_], Throwable]](xa: Transactor[F]): UserRepositoryDoobieInterpreter[F] =
    new UserRepositoryDoobieInterpreter(xa)
}
