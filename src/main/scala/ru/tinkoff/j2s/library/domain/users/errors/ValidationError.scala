package ru.tinkoff.j2s.library.domain.users.errors

import ru.tinkoff.j2s.library.domain.users.User

sealed trait ValidationError
case object UserNotFoundError extends ValidationError
case class UserAlreadyExistsError(user: User) extends ValidationError
case class UserAuthenticationFailedError(userName: String) extends ValidationError
