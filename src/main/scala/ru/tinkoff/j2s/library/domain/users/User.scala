package ru.tinkoff.j2s.library.domain.users

import cats.Applicative

import tsec.authorization.AuthorizationInfo

import ru.tinkoff.j2s.library.domain.auth.Role

case class User(
  id: Long,
  role: Role,
  userName: String,
  firstName: String,
  lastName: String,
  email: String,
  hash: String,
  phone: String
)

object User {
  implicit def authRole[F[_]](implicit F: Applicative[F]): AuthorizationInfo[F, Role, User] =
    new AuthorizationInfo[F, Role, User] {
      def fetchInfo(u: User): F[Role] = F.pure(u.role)
    }
}
