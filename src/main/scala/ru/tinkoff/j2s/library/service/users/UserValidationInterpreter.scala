package ru.tinkoff.j2s.library.service.users

import cats.Applicative
import cats.data.EitherT
import cats.implicits._

import ru.tinkoff.j2s.library.domain.users.User
import ru.tinkoff.j2s.library.domain.users.errors._
import ru.tinkoff.j2s.library.repository.users.UserRepository

class UserValidationInterpreter[F[_]: Applicative](userRepo: UserRepository[F]) extends UserValidation[F] {
  def userNameDoesNotExist(userName: String): EitherT[F, UserAlreadyExistsError, Unit] =
    userRepo
      .findByUserName(userName)
      .map(UserAlreadyExistsError)
      .toLeft(())

  def exists(userId: Long): EitherT[F, UserNotFoundError.type, Unit] =
    userRepo.get(userId)
            .toRight(UserNotFoundError)
            .void
}

object UserValidationInterpreter {
  def apply[F[_]: Applicative](repo: UserRepository[F]): UserValidation[F] =
    new UserValidationInterpreter[F](repo)
}
