package ru.tinkoff.j2s.library.service.users

import cats.data.EitherT

import ru.tinkoff.j2s.library.domain.users.User
import ru.tinkoff.j2s.library.domain.users.errors._

trait UserValidation[F[_]] {
  def userNameDoesNotExist(userName: String): EitherT[F, UserAlreadyExistsError, Unit]

  def exists(userId: Long): EitherT[F, UserNotFoundError.type, Unit]
}
