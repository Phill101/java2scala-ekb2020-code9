package ru.tinkoff.j2s.library.infrastructure.config

import cats.effect._

import pureconfig._
import pureconfig.generic.auto._
import pureconfig.module.cats._
import pureconfig.module.catseffect.syntax._
import pureconfig.module.enumeratum._

object ConfigModule {
  def apply[F[_]](blocker: Blocker)(implicit sync: Sync[F], cs: ContextShift[F]) =
    ConfigSource.defaultReference.loadF[F, Config](blocker)
}
