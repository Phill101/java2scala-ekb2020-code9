package ru.tinkoff.j2s.library.infrastructure.endpoints

import cats.syntax.all._
import cats.effect.Sync

import io.circe._
import io.circe.generic.semiauto._
import io.circe.syntax._

import org.http4s.{Request, Response, HttpRoutes, EntityDecoder}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

import tsec.authentication._
import tsec.jwt.algorithms.JWTMacAlgo

import ru.tinkoff.j2s.library.domain.auth.Role
import ru.tinkoff.j2s.library.domain.users.User
import ru.tinkoff.j2s.library.service.users.UserService
import ru.tinkoff.j2s.library.infrastructure.endpoints.Auth

class UserEndpoints[F[_]: Sync, Auth: JWTMacAlgo] extends Http4sDsl[F] {
  import serializers.UserSerializers._
  import UserEndpoints._

  implicit val createUserFormEntityDecoder: EntityDecoder[F, CreateUserForm] = jsonOf[F, CreateUserForm]
  implicit val userEntityDecoder: EntityDecoder[F, User] = jsonOf[F, User]

  object UserIdQueryParamMatcher extends QueryParamDecoderMatcher[Long]("id")
  object UserNameQueryParamMatcher extends QueryParamDecoderMatcher[String]("userName")

  object OptionalPageSizeMatcher extends OptionalQueryParamDecoderMatcher[Int]("pageSize")
  object OptionalOffsetMatcher extends OptionalQueryParamDecoderMatcher[Int]("offset")

  private def info(userService: UserService[F]): PartialFunction[Request[F], F[Response[F]]] = {
    case GET -> Root / "info" :? UserIdQueryParamMatcher(id) =>
      userService.getUser(id).foldF(
        error => NotFound(error.toString),
        user  => Ok(UserInfo(
                   id        = user.id,
                   userName  = user.userName,
                   firstName = user.firstName,
                   lastName  = user.lastName,
                   email     = user.email,
                   phone     = user.phone,
                 ).asJson)
      )
  }

  private def create(userService: UserService[F]): AuthEndpoint[F, Auth] = {
    case req @ POST -> Root asAuthed _ =>
      for {
        user <- req.request.as[CreateUserForm]
        resp <- userService.createUser(
                  role      = user.role,
                  userName  = user.userName,
                  firstName = user.firstName,
                  lastName  = user.lastName,
                  email     = user.email,
                  hash      = user.hash,
                  phone     = user.phone
                ).foldF(error => BadRequest(error.toString), u => Ok(u.asJson))
      } yield resp
  }

  private def get(userService: UserService[F]): AuthEndpoint[F, Auth] = {
    case GET -> Root / LongVar(id) asAuthed _ =>
      userService.getUser(id).foldF(
        error => NotFound(error.toString),
        user  => Ok(user.asJson)
      )
  }
  private def findByUserName(userService: UserService[F]): AuthEndpoint[F, Auth] = {
    case GET -> Root :? UserNameQueryParamMatcher(userName) asAuthed _ =>
      userService.getUserByName(userName).foldF(
        error => NotFound(error.toString),
        user  => Ok(user.asJson)
      )
  }
  private def update(userService: UserService[F]): AuthEndpoint[F, Auth] = {
    case req @ PUT -> Root / LongVar(id) asAuthed _ =>
      for {
        user <- req.request.as[User]
        resp <- userService.update(user).foldF(
                  error => NotFound(error.toString),
                  user  => Ok(user.asJson)
                )
      } yield resp
  }
  private def delete(userService: UserService[F]): AuthEndpoint[F, Auth] = {
    case DELETE -> Root / LongVar(id) asAuthed _ =>
      userService.deleteUser(id) *> Ok()
  }
  private def deleteByUserName(userService: UserService[F]): AuthEndpoint[F, Auth] = {
    case DELETE -> Root :? UserNameQueryParamMatcher(userName) asAuthed _ =>
      userService.deleteByUserName(userName) *> Ok()
  }

  private def getAll(userService: UserService[F]): AuthEndpoint[F, Auth] = {
    case GET -> Root :? OptionalPageSizeMatcher(pageSize) :? OptionalOffsetMatcher(offset) asAuthed _ =>
      userService.list(pageSize.getOrElse(10), offset.getOrElse(0))
                 .flatMap(list => Ok(list.asJson))
  }

  def endpoints(
    userService: UserService[F],
    auth: SecuredRequestHandler[F, Long, User, AugmentedJWT[Auth, Long]]
  ): HttpRoutes[F] = {
    val adminEndpoints =
      Auth.adminOnly {
        create(userService)
          .orElse(get(userService))
          .orElse(findByUserName(userService))
          .orElse(update(userService))
          .orElse(delete(userService))
          .orElse(deleteByUserName(userService))
          .orElse(getAll(userService))
      }

    val unauthEndpoints =
      HttpRoutes.of[F](info(userService))

    unauthEndpoints <+> auth.liftService(adminEndpoints)
  }
}
object UserEndpoints {
  def endpoints[F[_]: Sync, Auth: JWTMacAlgo](
    userService: UserService[F],
    auth: SecuredRequestHandler[F, Long, User, AugmentedJWT[Auth, Long]]
  ): HttpRoutes[F] =
    new UserEndpoints[F, Auth].endpoints(userService, auth)

  case class CreateUserForm(
    role: Role,
    userName: String,
    firstName: String,
    lastName: String,
    email: String,
    hash: String,
    phone: String
  )
  object CreateUserForm {
    implicit val decoder: Decoder[CreateUserForm] = deriveDecoder
  }

  case class UserInfo(
    id: Long,
    userName: String,
    firstName: String,
    lastName: String,
    email: String,
    phone: String,
  )
  object UserInfo {
    implicit val encoder: Encoder[UserInfo] = deriveEncoder
  }
}
