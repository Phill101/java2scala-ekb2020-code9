package ru.tinkoff.j2s.library.infrastructure.config

import scala.concurrent.duration.Duration

final case class ServerConfig(host: String, port: Int)

final case class DatabaseConnectionsConfig(poolSize: Int)
final case class DatabaseConfig(
    url: String,
    driver: String,
    user: String,
    password: String,
    connections: DatabaseConnectionsConfig,
)
final case class GoogleBooksConfig(
  readTimeout: Duration
)

final case class Config(db: DatabaseConfig, server: ServerConfig, googleBooks: GoogleBooksConfig)
