package ru.tinkoff.j2s.library.infrastructure.logging

import cats.effect.Sync
import cats.syntax.applicative._
import cats.syntax.functor._

import org.slf4j.Logger
import scala.reflect.ClassTag
import org.slf4j.LoggerFactory

class LogbackLog[F[_]](l: Logger)(implicit F: Sync[F]) extends Log[F] {
  override def trace(msg: String): F[Unit] =
    F.delay(l.trace(msg)).whenA(l.isTraceEnabled)
  override def trace(msg: String, th: Throwable): F[Unit] =
    F.delay(l.trace(msg, th)).whenA(l.isTraceEnabled)

  override def debug(msg: String): F[Unit] =
    F.delay(l.debug(msg)).whenA(l.isDebugEnabled)
  override def debug(msg: String, th: Throwable): F[Unit] =
    F.delay(l.debug(msg, th)).whenA(l.isDebugEnabled)

  override def info(msg: String): F[Unit] =
    F.delay(l.info(msg)).whenA(l.isInfoEnabled)
  override def info(msg: String, th: Throwable): F[Unit] =
    F.delay(l.info(msg, th)).whenA(l.isInfoEnabled)

  override def warn(msg: String): F[Unit] =
    F.delay(l.warn(msg)).whenA(l.isWarnEnabled)
  override def warn(msg: String, th: Throwable): F[Unit] =
    F.delay(l.warn(msg, th)).whenA(l.isWarnEnabled)

  override def error(msg: String): F[Unit] =
    F.delay(l.error(msg)).whenA(l.isErrorEnabled)
  override def error(msg: String, th: Throwable): F[Unit] =
    F.delay(l.error(msg, th)).whenA(l.isErrorEnabled)
}

object LogbackLog {
  def apply[F[_]: Sync](l: Logger) = new LogbackLog[F](l)
  def apply[F[_]: Sync]            = new PartialyApplied[F]

  class PartialyApplied[F[_]: Sync] {
    def forName(name: String): F[Log[F]] =
      Sync[F].delay(LoggerFactory.getLogger(name))
             .map(new LogbackLog[F](_))

    def forService[A: ClassTag]: F[Log[F]] =
      Sync[F].delay(LoggerFactory.getLogger(implicitly[ClassTag[A]].runtimeClass))
             .map(new LogbackLog[F](_))
  }
}
